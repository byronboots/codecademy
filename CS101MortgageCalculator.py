input_principal = int(input('How much of a mortgage are you looking to take out? '))
input_interest = float(input('Great, and at what annual rate? '))
input_years = int(input('And how long a term for the mortgage? '))

# Function for calculating the mortgage payment
def calculate_mortgage(principal, interest, years):
    # monthly rate from annual rate
    interest_rate = interest/100/12
    # total number of payments
    num_payments = years * 12
    # calculate payment
    payment = (interest_rate/(1-(1+interest_rate)**(-num_payments)))*principal
    return payment

# Calculates total payment based on inputted values, rounds to 2 decimal places
total_payment = round(calculate_mortgage(input_principal, input_interest, input_years),2)
# print(total_payment)

# Organizes and displays message explaining total payment and inputs that resulted in the calculation
amount_message = 'Your monthly payment for a mortgage with a ${principal} principal, {interest} percent interest rate and {years} year term will be ${payment}.'.format(principal = input_principal, interest = input_interest, years = input_years, payment = total_payment)
print(amount_message)